package pt.vamosmarmitar.backend.config;

import org.springframework.boot.autoconfigure.domain.*;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.*;
import org.springframework.mail.javamail.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

@Configuration
@EntityScan("pt.vamosmarmitar.backend.model.entity")
@ComponentScan({"pt.vamosmarmitar.backend.service"})
@EnableJpaRepositories(basePackages = { "pt.vamosmarmitar.backend.model.repository" })
@EnableTransactionManagement
public class AbstractConfig {

//    @Override
//    public void run(String... args) {
//        System.out.println("Sending Email...");
//        try {
//            emailService.sendEmail();
//            //sendEmailWithAttachment();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println("Done");
//    }


    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("jun.oliveira10@gmail.com");
        mailSender.setPassword("Eli@$9254");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

}
