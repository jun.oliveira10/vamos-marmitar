package pt.vamosmarmitar.backend.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import pt.vamosmarmitar.backend.exception.BusinessException;
import pt.vamosmarmitar.backend.model.builder.RegisterBuilder;
import pt.vamosmarmitar.backend.model.dto.MessageDTO;
import pt.vamosmarmitar.backend.model.dto.RegisterDTO;
import pt.vamosmarmitar.backend.model.dto.UserDTO;
import pt.vamosmarmitar.backend.model.enums.UserStatus;
import pt.vamosmarmitar.backend.model.repository.RegisterRepository;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Locale;

@Service
public class RegisterService {

    private static final String KEY_USER_NOT_FOUND = "user.usernotfound";
    private static final String ID_MESSAGE_SUB = "mail.user.validation.subject";
    private static final String ID_MESSAGE_CONTENT = "mail.user.validation.message";
    private static final String ID_MESSAGE_LINK = "mail.user.validation.link";

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private RegisterRepository registerRepository;

    @Autowired
    private RegisterBuilder builder;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    public UserDTO validateUser(RegisterDTO registerDTO) throws BusinessException {
        String userId = registerRepository.findById(registerDTO.getId())
                .orElseThrow(() -> new BusinessException(KEY_USER_NOT_FOUND))
                .getUserId();

        UserDTO user = userService.getById(userId);
        if(user == null) {
            throw new BusinessException(KEY_USER_NOT_FOUND);
        }
        user.setStatus(UserStatus.VALID.getValue());
        return userService.save(user);
    }

    public RegisterDTO requestValitation(RegisterDTO registerDTO) throws BusinessException {
        RegisterDTO register = builder.getDto(registerRepository.save(builder.getEntity(registerDTO)));

        UserDTO user = userService.getById(register.getUserId());
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setUserDTO(user);
        messageDTO.setResource("email/validation.html");
        messageDTO.setSubject(messageSource.getMessage(ID_MESSAGE_SUB, null, Locale.getDefault()));
        messageDTO.getParams().add(messageSource.getMessage(ID_MESSAGE_SUB, null, Locale.getDefault()));
        messageDTO.getParams().add(messageSource.getMessage(ID_MESSAGE_CONTENT, null, Locale.getDefault()));
        messageDTO.getParams().add(messageSource.getMessage(ID_MESSAGE_LINK, new Object[]{register.getId()}, Locale.getDefault()));

        try {
            emailService.sendEmailHTML(messageDTO);
        } catch (MessagingException | IOException e) {
            throw new BusinessException(e);
        }

        return register;
    }

}

