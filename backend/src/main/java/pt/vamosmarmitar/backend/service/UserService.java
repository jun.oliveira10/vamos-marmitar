package pt.vamosmarmitar.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import pt.vamosmarmitar.backend.exception.BusinessException;
import pt.vamosmarmitar.backend.model.builder.UserBuilder;
import pt.vamosmarmitar.backend.model.dto.RegisterDTO;
import pt.vamosmarmitar.backend.model.dto.UserDTO;
import pt.vamosmarmitar.backend.model.entity.UserEntity;
import pt.vamosmarmitar.backend.model.enums.UserStatus;
import pt.vamosmarmitar.backend.model.enums.UserType;
import pt.vamosmarmitar.backend.model.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserBuilder builder;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private RegisterService registerService;

    private static String KEY_MESSAGE_USER_NOT_FOUD = "user.usernotfound";

    public List<UserDTO> getAll() {
        List<UserDTO> ret = new ArrayList<>();
        getAllEntities()
                .stream()
                .map(builder::getDto)
                .forEach(ret::add);
        return ret;
    }

    public List<UserEntity> getAllEntities() {
        List<UserEntity> ret = new ArrayList<>();
        repository.findAll().forEach(ret::add);
        return ret;
    }

    public UserDTO save(UserDTO dto) {
        UserEntity entity = builder.getEntity(dto);
        return  builder.getDto(repository.save(entity));
    }

    public UserDTO newUser(UserDTO dto) throws BusinessException {
        dto.setType(UserType.CLIENT);
        UserDTO newUser = save(dto);
        registerService.requestValitation(new RegisterDTO(newUser.getId()));
        return newUser;
    }

    public UserDTO validateLogin(UserDTO dto) throws BusinessException {
        List<UserEntity> allByEmail = repository.findAllByEmail(dto.getEmail());
        UserEntity userEntity = allByEmail
                .stream()
                .findFirst()
                .orElse(null);
        if(userEntity != null
                && UserStatus.VALID.getValue().equals(userEntity.getStatus())
                && userEntity.getPassword().equals(dto.getPassword())) {
            return builder.getDto(userEntity);
        } else {
            throw new BusinessException(messageSource.getMessage(KEY_MESSAGE_USER_NOT_FOUD, null , Locale.getDefault()));
        }
    }

    public UserDTO getUserAdmin() {
        return  builder.getDto(getAllEntities()
                .stream()
                .filter(ent -> ent.getType().equals(UserType.ADMIN.getValue()))
                .findFirst()
                .get());
    }

    public UserDTO getById(String id) {
        return  builder.getDto(repository.findById(id)
                .get());
    }

}
