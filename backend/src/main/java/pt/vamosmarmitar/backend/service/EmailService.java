package pt.vamosmarmitar.backend.service;


import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.MessageSource;
import org.springframework.core.io.*;
import org.springframework.mail.*;
import org.springframework.mail.javamail.*;
import org.springframework.stereotype.Service;
import pt.vamosmarmitar.backend.model.dto.MessageDTO;
import pt.vamosmarmitar.backend.model.dto.UserDTO;

import javax.annotation.*;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.stream.Stream;

@Service
public class EmailService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserService userService;

//    @PostConstruct
//    private void init(){
//        sendEmail();
//    }

    public void sendEmail(String keySubject, String keyMessage, String to, Object[] param) {
        String message = messageSource.getMessage(keyMessage, param, Locale.getDefault());
        String subject = messageSource.getMessage(keySubject, param, Locale.getDefault());
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(to);
        msg.setSubject(subject);
        msg.setText(message);
        javaMailSender.send(msg);
    }


    public void sendBackupAdmin() throws MessagingException {
        UserDTO admin = userService.getUserAdmin();
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(admin.getEmail());
        helper.setSubject("Backup database");
        helper.setText("<h1>Database em anexo</h1>", true);
        helper.addAttachment("init.json", new ClassPathResource("init.json"));
        javaMailSender.send(msg);

    }

    public void sendEmailHTML(MessageDTO messageDTO) throws MessagingException, IOException {
        UserDTO userDTO = messageDTO.getUserDTO();
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(userDTO.getEmail());
        helper.setSubject(messageDTO.getSubject());

        ClassPathResource classPathResource = new ClassPathResource(messageDTO.getResource());

        String emailHtml = FileUtils.readFileToString(classPathResource.getFile(), StandardCharsets.UTF_8);
        emailHtml = MessageFormat.format(emailHtml, messageDTO.getParams().toArray());
        helper.setText(emailHtml, true);
        javaMailSender.send(msg);
    }

//    public void sendEmailWithAttachment() throws MessagingException, IOException {
//
//        MimeMessage msg = javaMailSender.createMimeMessage();
//
//        // true = multipart message
//        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
//
//        helper.setTo("to_@email");
//
//        helper.setSubject("Testing from Spring Boot");
//
//        // default = text/plain
//        //helper.setText("Check attachment for image!");
//
//        // true = text/html
//        helper.setText("<h1>Check attachment for image!</h1>", true);
//
//        // hard coded a file path
//        //FileSystemResource file = new FileSystemResource(new File("path/android.png"));
//
//        helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));
//
//        javaMailSender.send(msg);
//
//    }


}

