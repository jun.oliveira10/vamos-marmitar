package pt.vamosmarmitar.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import pt.vamosmarmitar.backend.model.builder.OrderBuilder;
import pt.vamosmarmitar.backend.model.dto.ItemDTO;
import pt.vamosmarmitar.backend.model.dto.MessageDTO;
import pt.vamosmarmitar.backend.model.dto.OrderDTO;
import pt.vamosmarmitar.backend.model.dto.UserDTO;
import pt.vamosmarmitar.backend.model.entity.ItemEntity;
import pt.vamosmarmitar.backend.model.entity.OrderEntity;
import pt.vamosmarmitar.backend.model.enums.OrderStatus;
import pt.vamosmarmitar.backend.model.repository.OrderRepository;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class OrderService {

    @Autowired
    private MessageSource messageSource;

    private static final String KEY_EMAIL_TITLE = "mail.title";

    private static final String KEY_EMAIL_GREETINGS = "mail.greetings";

    private static final String SUB_ORDERSTATE = "mail.subject.orderstate";

    private static final String KEY_ORDERSTATE_DOING_MSG1 = "mail.orderstate.doing.msg1";
    private static final String KEY_ORDERSTATE_DOING_MSG2 = "mail.orderstate.doing.msg2";

    private static final String KEY_ORDERSTATE_DONE_MSG1 = "mail.orderstate.done.msg1";
    private static final String KEY_ORDERSTATE_DONE_MSG2 = "mail.orderstate.done.msg2";

    private static final String KEY_ORDERSTATE_NEW_MSG1 = "mail.orderstate.new.msg1";
    private static final String KEY_ORDERSTATE_NEW_MSG2 = "mail.orderstate.new.msg2";

    @Autowired
    private OrderRepository repository;

    @Autowired
    private OrderBuilder builder;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    public List<OrderDTO> getAll() {
        List<OrderDTO> ret = new ArrayList<>();
        getAllEntities()
                .stream()
                .map(builder::getDto)
                .forEach(ret::add);
        return ret;
    }

    private List<OrderEntity> getAllEntities() {
        List<OrderEntity> ret = new ArrayList<>();
        repository.findAll().forEach(ret::add);
        return  ret;
    }

    public OrderDTO save(OrderDTO dto)  {
        return save(dto, OrderStatus.NEW);
    }

    private OrderDTO save(OrderDTO dto, OrderStatus status)  {
        OrderEntity entity = builder.getEntity(dto);
        entity.setStatus(status.getValue());
        return builder.getDto(repository.save(entity));
    }

    public OrderDTO newOrder(OrderDTO dto) throws MessagingException, IOException {
        OrderDTO saved = this.save(dto);
        UserDTO user = userService.getById(saved.getUserId());
        if(user != null) {
            sendEmail(user, saved, KEY_ORDERSTATE_NEW_MSG1);
        }
        return saved;
    }

    public OrderDTO prepareOder(OrderDTO dto) throws MessagingException, IOException {
        OrderDTO saved = save(dto, OrderStatus.DOING);
        UserDTO user = userService.getById(saved.getUserId());
        if(user != null) {
            sendEmail(user, saved, KEY_ORDERSTATE_DOING_MSG1);
        }
        return saved;
    }

    public OrderDTO finishOrder(OrderDTO dto) throws MessagingException, IOException {
        OrderDTO saved = save(dto, OrderStatus.DONE);
        UserDTO user = userService.getById(saved.getUserId());
        if(user != null) {
            sendEmail(user, saved, KEY_ORDERSTATE_DONE_MSG1);
        }
        return saved;
    }

    private void sendEmail(UserDTO user, OrderDTO dto, String idMessage) throws MessagingException, IOException {
        MessageDTO message = new MessageDTO();
        message.setUserDTO(user);
        String template = "email/example.html";
        String subject = messageSource.getMessage(SUB_ORDERSTATE, null, Locale.getDefault());
        String title = messageSource.getMessage(KEY_EMAIL_TITLE, null, Locale.getDefault());
        String greetings = messageSource.getMessage(KEY_EMAIL_GREETINGS, null, Locale.getDefault());
        String param = "";
        String param2 = "";
        // Params
        switch (idMessage) {
            case KEY_ORDERSTATE_NEW_MSG1:
                param = messageSource.getMessage(KEY_ORDERSTATE_NEW_MSG1, new Object[]{dto.getDateCreation()}, Locale.getDefault());
                param2 = messageSource.getMessage(KEY_ORDERSTATE_NEW_MSG2, new Object[]{dto.getId()}, Locale.getDefault());
                break;
            case KEY_ORDERSTATE_DOING_MSG1:
                param = messageSource.getMessage(KEY_ORDERSTATE_DOING_MSG1, new Object[]{}, Locale.getDefault());
                param2 = messageSource.getMessage(KEY_ORDERSTATE_DOING_MSG2, new Object[]{dto.getDateCreation()}, Locale.getDefault());
                break;
            case KEY_ORDERSTATE_DONE_MSG1:
                param = messageSource.getMessage(KEY_ORDERSTATE_DONE_MSG1, new Object[]{}, Locale.getDefault());
                param2 = messageSource.getMessage(KEY_ORDERSTATE_DONE_MSG2, new Object[]{dto.getDateCreation()}, Locale.getDefault());
                break;
            default:
                break;
        }
        message.setSubject(subject);
        message.getParams().add(title);
        message.getParams().add(greetings);
        message.getParams().add(param);
        message.getParams().add(param2);
        message.setResource(template);
        emailService.sendEmailHTML(message);
    }

}
