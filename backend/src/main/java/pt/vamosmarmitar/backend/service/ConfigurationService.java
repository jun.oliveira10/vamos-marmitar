package pt.vamosmarmitar.backend.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.*;
import org.springframework.core.io.*;
import org.springframework.stereotype.Service;
import pt.vamosmarmitar.backend.model.dto.ConfigDTO;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.stream.*;

@Service
public class ConfigurationService {

    @Value("classpath:init.json")
    private Resource resourceInitFile;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ItemService itemService;

    @PostConstruct
    public void init() {
        System.out.println("Init called");
        String initString = "{}";
        ConfigDTO result = null;
        try ( BufferedReader reader = new BufferedReader(
                new InputStreamReader(resourceInitFile.getInputStream())) ) {
            initString = reader.lines()
                    .collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        result  = new Gson().fromJson(initString, ConfigDTO.class);

        if(result != null) {
            result.getUsers().forEach(userService::save);
            result.getOrders().forEach(orderService::save);
            result.getItems().forEach(itemService::save);
        } else {
            throw new RuntimeException("Init Error");
        }
    }

}
