package pt.vamosmarmitar.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.vamosmarmitar.backend.model.builder.ItemBuilder;
import pt.vamosmarmitar.backend.model.dto.ItemDTO;
import pt.vamosmarmitar.backend.model.dto.UserDTO;
import pt.vamosmarmitar.backend.model.entity.ItemEntity;
import pt.vamosmarmitar.backend.model.repository.ItemRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    @Autowired
    private ItemBuilder builder;

    public List<ItemDTO> getAll() {
        List<ItemDTO> ret = new ArrayList<>();
        getAllEntities()
                .stream()
                .map(builder::getDto)
                .forEach(ret::add);
        return ret;
    }

    private List<ItemEntity> getAllEntities() {
        List<ItemEntity> ret = new ArrayList<>();
        repository.findAll().forEach(ret::add);
        return  ret;
    }

    public ItemDTO getById(String id) {
        return builder.getDto(repository.findById(id)
                .orElse(new ItemEntity()));
    }

    public ItemDTO save(ItemDTO dto) {
        ItemEntity entity = builder.getEntity(dto);

        return builder.getDto(repository.save(entity));
    }

    public ItemDTO newItem(ItemDTO item) {
        return save(item);
    }

    public boolean delete(String idItem) {
        repository.deleteById(idItem);
        return true;
    }
}
