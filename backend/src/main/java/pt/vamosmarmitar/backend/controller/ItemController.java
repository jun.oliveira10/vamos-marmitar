package pt.vamosmarmitar.backend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pt.vamosmarmitar.backend.model.dto.ItemDTO;
import pt.vamosmarmitar.backend.service.ItemService;

import java.util.List;

@RestController
@RequestMapping("Item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @GetMapping("/getAll")
    public List<ItemDTO> getAll() {
        return itemService.getAll();
    }

    @PostMapping("/new")
    public ItemDTO save(@RequestBody ItemDTO item) {
        return itemService.newItem(item);
    }

    @DeleteMapping
    public boolean remove(@RequestParam("idItem") String idItem) {
        return itemService.delete(idItem);
    }

}
