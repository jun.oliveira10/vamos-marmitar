package pt.vamosmarmitar.backend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pt.vamosmarmitar.backend.exception.BusinessException;
import pt.vamosmarmitar.backend.model.dto.RegisterDTO;
import pt.vamosmarmitar.backend.model.dto.UserDTO;
import pt.vamosmarmitar.backend.service.RegisterService;
import pt.vamosmarmitar.backend.service.UserService;

import java.util.List;

@RestController
@RequestMapping("User")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RegisterService registerService;

    @GetMapping("/getAll")
    public List<UserDTO> getAll() {
        return userService.getAll();
    }

    @PostMapping("/new")
    public UserDTO save(@RequestBody UserDTO user) throws BusinessException {
        return userService.newUser(user);
    }

    @PostMapping("/login")
    public UserDTO login(@RequestBody UserDTO user) throws BusinessException {
        return userService.validateLogin(user);
    }

    @PostMapping("/validate")
    public UserDTO register(@RequestParam(value = "registerId") int registerId) throws BusinessException {
        RegisterDTO registerDTO = new RegisterDTO();
        registerDTO.setId(registerId);
        return registerService.validateUser(registerDTO);
    }

}
