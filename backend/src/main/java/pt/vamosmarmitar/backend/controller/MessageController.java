package pt.vamosmarmitar.backend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pt.vamosmarmitar.backend.model.dto.MailAdminDTO;
import pt.vamosmarmitar.backend.model.dto.MessageDTO;
import pt.vamosmarmitar.backend.model.dto.UserDTO;
import pt.vamosmarmitar.backend.service.EmailService;
import pt.vamosmarmitar.backend.service.UserService;

import javax.mail.MessagingException;
import java.io.IOException;

@RestController
@RequestMapping("/Message")
public class MessageController {

    private static final String ID_MESSAGE_SUB = "mail.admin.subject";
    private static final String ID_MESSAGE_ADMIN = "mail.admin.message";

    @Autowired
    private EmailService service;

    @Autowired
    private UserService userService;

    @PostMapping("/mail-admin")
    public boolean mailForAdmin(@RequestBody MailAdminDTO objectMessage) {
        UserDTO userAdmin = userService.getUserAdmin();
        Object[] param = { objectMessage.getName(), objectMessage.getEmail(), objectMessage.getMessage() };
        service.sendEmail(ID_MESSAGE_SUB, ID_MESSAGE_ADMIN, userAdmin.getEmail(), param);
        return true;
    }

    @GetMapping("/mail-backup")
    public boolean backup() throws MessagingException {
        service.sendBackupAdmin();
        return true;
    }

    @GetMapping("/mail-order-test")
    public boolean orderTest() throws MessagingException, IOException {
        MessageDTO message = new MessageDTO();
        message.setUserDTO(userService.getUserAdmin());
        message.setSubject("Testing html");
        message.setResource("email/example.html");
        message.getParams().add("Vamos Marmitar");
        message.getParams().add("Estimado(a) Cliente");
        message.getParams().add("Estamos a preparar seu pedido numero: 123321");
        message.getParams().add("A previsão de chegada e : 01/12/2019 11:12:00");
        service.sendEmailHTML(message);
        return true;
    }

}
