package pt.vamosmarmitar.backend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pt.vamosmarmitar.backend.model.dto.OrderDTO;
import pt.vamosmarmitar.backend.service.OrderService;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("Order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/getAll")
    public List<OrderDTO> getAll() {
        return orderService.getAll();
    }

    @PostMapping("/new")
    public OrderDTO newOrder(@RequestBody OrderDTO order) throws MessagingException, IOException {
        return orderService.newOrder(order);
    }

    @PostMapping("/prepare")
    public OrderDTO prepareOrder(@RequestBody OrderDTO order) throws MessagingException, IOException {
        return orderService.prepareOder(order);
    }

    @PostMapping("/finish")
    public OrderDTO finishOrder(@RequestBody OrderDTO order) throws MessagingException, IOException {
        return orderService.finishOrder(order);
    }

}
