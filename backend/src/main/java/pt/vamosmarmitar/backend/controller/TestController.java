package pt.vamosmarmitar.backend.controller;


import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import pt.vamosmarmitar.backend.model.dto.*;
import pt.vamosmarmitar.backend.model.entity.*;
import pt.vamosmarmitar.backend.service.*;

import java.util.*;

@RestController
@RequestMapping("Test")
public class TestController {

    @Autowired
    private TestService service;

    @GetMapping
    public String getHello() {
        return  "{ path: \"products\", component: \"ProductComponent\", data: { title: \"Product List Test Heroku\" }  }";
    }

//    @GetMapping("/all")
//    public List<TestEntity> getAll() {
//        return service.getAll();
//    }
//
//    @PostMapping
//    public boolean save(@RequestBody TestDTO entity) {
//        return service.save(entity);
//    }

}
