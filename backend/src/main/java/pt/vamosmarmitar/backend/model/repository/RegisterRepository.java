package pt.vamosmarmitar.backend.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pt.vamosmarmitar.backend.model.entity.OrderEntity;
import pt.vamosmarmitar.backend.model.entity.RegisterEntity;

@Repository
public interface RegisterRepository extends CrudRepository<RegisterEntity, Integer> {

}

