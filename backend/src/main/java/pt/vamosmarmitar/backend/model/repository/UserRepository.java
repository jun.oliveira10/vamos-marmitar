package pt.vamosmarmitar.backend.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pt.vamosmarmitar.backend.model.entity.UserEntity;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {

    List<UserEntity> findAllByEmail(String email);

}

