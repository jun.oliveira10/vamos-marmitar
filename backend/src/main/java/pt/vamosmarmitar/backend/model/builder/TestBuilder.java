package pt.vamosmarmitar.backend.model.builder;

import org.springframework.stereotype.*;
import pt.vamosmarmitar.backend.model.dto.*;
import pt.vamosmarmitar.backend.model.entity.*;

@Component
public class TestBuilder extends AbstractBuilder<TestDTO, TestEntity> {

    @Override
    public TestDTO getDto(TestEntity entity) {
        TestDTO dto = new TestDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

    @Override
    public TestEntity getEntity(TestDTO dto) {
        TestEntity entity = new TestEntity();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }
}
