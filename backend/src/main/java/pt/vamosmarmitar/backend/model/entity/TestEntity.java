package pt.vamosmarmitar.backend.model.entity;

import com.google.gson.Gson;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.*;

@Entity
public class TestEntity implements Serializable, IEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "\"ID\"")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
