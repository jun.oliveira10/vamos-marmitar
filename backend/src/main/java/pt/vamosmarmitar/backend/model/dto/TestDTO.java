package pt.vamosmarmitar.backend.model.dto;

public class TestDTO implements Dto {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
