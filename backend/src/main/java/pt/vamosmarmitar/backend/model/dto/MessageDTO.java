package pt.vamosmarmitar.backend.model.dto;

import java.util.ArrayList;
import java.util.List;

public class MessageDTO {

    private UserDTO userDTO;

    private String subject;

    private String resource;

    private List<String> params = new ArrayList<>();

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }
}
