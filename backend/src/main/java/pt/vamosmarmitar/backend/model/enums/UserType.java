package pt.vamosmarmitar.backend.model.enums;

import com.google.gson.Gson;

import java.util.Arrays;

public enum UserType {

    ADMIN("Admin", "A"),
    CLIENT("Client", "C");

    private String desc;
    private String value;

    UserType(String desc, String value) {
        this.desc = desc;
        this.value = value;
    }

    public static UserType getByValue(String value){
        return Arrays.stream(values())
                .filter(o -> o.getValue().equals(value))
                .findFirst()
                .orElse(CLIENT);
    }

    public String getDesc() {
        return desc;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
