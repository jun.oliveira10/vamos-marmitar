package pt.vamosmarmitar.backend.model.dto;

import com.google.gson.Gson;

public class RegisterDTO {

    private static final long serialVersionUID = 1L;

    private int id;

    private String userId;

    public RegisterDTO() { }

    public RegisterDTO(String userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
