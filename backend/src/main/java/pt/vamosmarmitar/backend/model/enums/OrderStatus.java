package pt.vamosmarmitar.backend.model.enums;

import com.google.gson.Gson;

import java.util.Arrays;

public enum OrderStatus {

    NEW( "N"),
    DOING( "D"),
    DONE( "O"),
    EXPIRED( "E");

    private String value;

    OrderStatus(String value) {
        this.value = value;
    }

    public static OrderStatus getByValue(String value){
        return Arrays.stream(values())
                .filter(o -> o.getValue().equals(value))
                .findFirst()
                .orElse(NEW);
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
