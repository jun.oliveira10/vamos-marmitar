package pt.vamosmarmitar.backend.model.builder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pt.vamosmarmitar.backend.model.dto.ItemDTO;
import pt.vamosmarmitar.backend.model.dto.OrderDTO;
import pt.vamosmarmitar.backend.model.entity.OrderEntity;
import pt.vamosmarmitar.backend.service.ItemService;

import java.util.stream.Collectors;

@Component
public class OrderBuilder extends AbstractBuilder<OrderDTO, OrderEntity> {

    @Autowired
    private ItemService itemService;

    @Override
    public OrderDTO getDto(OrderEntity entity) {
        OrderDTO dto = new OrderDTO();
        dto.setId(entity.getId());
        dto.setDateCreation(entity.getDateCreation());
        dto.setUserId(entity.getUserId());
//        dto.setItems(entity.getItems()
//                .stream()
//                .map(iTemId -> itemService.getById(iTemId))
//                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public OrderEntity getEntity(OrderDTO dto) {
        OrderEntity entity = new OrderEntity();
        entity.setId(dto.getId());
        entity.setDateCreation(dto.getDateCreation());
        entity.setUserId(dto.getUserId());
//        entity.setItems(dto.getItems()
//                .stream()
//                .map(ItemDTO::getId)
//                .collect(Collectors.toList()));
        return entity;
    }
}
