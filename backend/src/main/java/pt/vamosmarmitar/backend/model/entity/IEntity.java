package pt.vamosmarmitar.backend.model.entity;

public interface IEntity {

    String getId();

    void setId(String id);

}
