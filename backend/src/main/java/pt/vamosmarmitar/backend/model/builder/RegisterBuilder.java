package pt.vamosmarmitar.backend.model.builder;

import org.springframework.stereotype.Component;
import pt.vamosmarmitar.backend.model.dto.RegisterDTO;
import pt.vamosmarmitar.backend.model.entity.RegisterEntity;

@Component
public class RegisterBuilder {

    public RegisterDTO getDto(RegisterEntity entity) {
        RegisterDTO dto = new RegisterDTO();
        dto.setId(entity.getId());
        dto.setUserId(entity.getUserId());
        return dto;
    }

    public RegisterEntity getEntity(RegisterDTO dto) {
        RegisterEntity entity = new RegisterEntity();
        entity.setId(dto.getId());
        entity.setUserId(dto.getUserId());
        return entity;
    }
}
