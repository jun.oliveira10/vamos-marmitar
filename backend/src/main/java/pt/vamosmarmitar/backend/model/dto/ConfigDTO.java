package pt.vamosmarmitar.backend.model.dto;

import java.util.ArrayList;
import java.util.List;

public class ConfigDTO {

    private List<UserDTO> users = new ArrayList<>();

    private List<OrderDTO> orders = new ArrayList<>();

    private List<ItemDTO> items = new ArrayList<>();

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }
}
