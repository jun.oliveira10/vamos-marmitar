package pt.vamosmarmitar.backend.model.builder;

import org.springframework.stereotype.Component;
import pt.vamosmarmitar.backend.model.dto.ItemDTO;
import pt.vamosmarmitar.backend.model.entity.ItemEntity;

@Component
public class ItemBuilder extends AbstractBuilder<ItemDTO, ItemEntity> {

    @Override
    public ItemDTO getDto(ItemEntity entity) {
        ItemDTO dto = new ItemDTO();
        dto.setId(entity.getId());
        dto.setDescription(entity.getDescription());
        dto.setPhoto(entity.getPhoto());
        dto.setTitle(entity.getTitle());
        return dto;
    }

    @Override
    public ItemEntity getEntity(ItemDTO dto) {
        ItemEntity entity = new ItemEntity();
        entity.setId(dto.getId());
        entity.setDescription(dto.getDescription());
        entity.setPhoto(dto.getPhoto());
        entity.setTitle(dto.getTitle());
        return entity;
    }
}

