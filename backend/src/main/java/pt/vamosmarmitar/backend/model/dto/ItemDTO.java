package pt.vamosmarmitar.backend.model.dto;

import com.google.gson.Gson;

public class ItemDTO implements Dto {

    private static final long serialVersionUID = 1L;

    private String id;

    private String description;

    private String photo;

    private String title;


    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
