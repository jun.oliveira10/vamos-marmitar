package pt.vamosmarmitar.backend.model.builder;

import org.springframework.stereotype.Component;
import pt.vamosmarmitar.backend.model.dto.UserDTO;
import pt.vamosmarmitar.backend.model.entity.UserEntity;
import pt.vamosmarmitar.backend.model.enums.UserStatus;
import pt.vamosmarmitar.backend.model.enums.UserType;

@Component
public class UserBuilder extends AbstractBuilder<UserDTO, UserEntity> {

    @Override
    public UserDTO getDto(UserEntity entity) {
        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setType(UserType.getByValue(entity.getType()));
        dto.setStatus(entity.getStatus() != null ? entity.getStatus() : UserStatus.NEW.getValue());
        return dto;
    }

    @Override
    public UserEntity getEntity(UserDTO dto) {
        UserEntity entity = new UserEntity();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setType(dto.getType().getValue());
        entity.setStatus(dto.getStatus() != null ? dto.getStatus() : UserStatus.NEW.getValue());
        return entity;
    }
}
