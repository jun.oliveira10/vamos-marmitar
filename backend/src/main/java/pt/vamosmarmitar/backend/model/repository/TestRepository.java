package pt.vamosmarmitar.backend.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pt.vamosmarmitar.backend.model.entity.TestEntity;

@Repository
public interface TestRepository extends CrudRepository<TestEntity, String> {

}

