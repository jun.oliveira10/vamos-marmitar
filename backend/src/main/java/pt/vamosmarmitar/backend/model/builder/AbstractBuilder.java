package pt.vamosmarmitar.backend.model.builder;

import pt.vamosmarmitar.backend.model.dto.*;
import pt.vamosmarmitar.backend.model.entity.*;

import java.util.*;
import java.util.stream.*;

public abstract class AbstractBuilder<DTO extends Dto, ENTITY extends IEntity> {


    public abstract DTO getDto(ENTITY entity);
    public abstract ENTITY getEntity(DTO dto);

    public List<DTO> getListDto(List<ENTITY> list) {
        return list.stream()
                .map(this::getDto)
                .collect(Collectors.toList());
    }

    public List<ENTITY> getListEntity(List<DTO> list) {
        return list.stream()
                .map(this::getEntity)
                .collect(Collectors.toList());
    }

}
