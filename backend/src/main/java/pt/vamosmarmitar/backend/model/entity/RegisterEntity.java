package pt.vamosmarmitar.backend.model.entity;

import com.google.gson.Gson;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class RegisterEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "\"ID\"")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
