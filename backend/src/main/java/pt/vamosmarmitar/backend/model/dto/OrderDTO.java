package pt.vamosmarmitar.backend.model.dto;

import com.google.gson.Gson;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class OrderDTO implements Dto {

    private static final long serialVersionUID = 1L;

    private String id;

    private String userId;

    private Instant dateCreation;

    private List<ItemDTO> items = new ArrayList<>();

    public OrderDTO() {
        dateCreation = Instant.now();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
