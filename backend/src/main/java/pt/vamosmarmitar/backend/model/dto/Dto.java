package pt.vamosmarmitar.backend.model.dto;

import java.io.Serializable;

public interface Dto extends Serializable {

    String getId();

    void setId(String id);

}
