import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app.routing';

import { NgxSpinnerModule } from "ngx-spinner";

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ComponentsModule } from './components/components.module';
import { ExamplesModule } from './examples/examples.module';

import { TestService } from './service/test.service';
import { EmailService } from './service/email.service';
import { CartService } from './service/cart.service';
import { LoginService } from './service/login.service';
import { UserService } from './service/user.service';
import { PedidoService } from './service/pedido.service';
import { ItemService } from './service/item.service';
import { MessageService } from './service/message.service';
import { AdminGuard } from './service/guard/admin-guard.service';
import { ClientGuard } from './service/guard/client-guard.service';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ComponentsModule,
    ExamplesModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  providers: [
    TestService,
    EmailService,
    CartService,
    LoginService,
    UserService,
    PedidoService,
    ItemService,
    MessageService,
    AdminGuard,
    ClientGuard,
    NgbActiveModal
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
