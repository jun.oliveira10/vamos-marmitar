import { Component, ElementRef, ViewChild } from '@angular/core';

import { ItemMenu } from '../../model/item-menu';

import { TestService } from '../../service/test.service';
import { CartService } from '../../service/cart.service';
import { User } from '../../model/user';
import { LoginService } from '../../service/login.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import * as _ from "lodash";
import { ItemService } from '../../service/item.service';

@Component({
  selector: 'app-cardapio',
  templateUrl: './cardapio.component.html',
  styleUrls: ['./cardapio.component.scss']
})
export class CardapioComponent {

  @ViewChild("addItemModal") modal: ElementRef;
  public modalItem: ItemMenu = _.defaults();
  public quantidade: number = 1;

  public user: User;
  private size = 3;
  private pageNumber = 0;
  public filter = "";
  public menuListPage1: ItemMenu[];
  public menuListPage2: ItemMenu[];
  public menuListPage3: ItemMenu[];
  public menuListPageAdd: ItemMenu[];
  public more: boolean;
  modalReference: NgbModalRef;

  constructor(private cartService: CartService,
              private loginService : LoginService,
              private modalService: NgbModal,
              private itemService: ItemService,
              private testService: TestService) {
    this.loginService.getUserSession().subscribe(userSession => this.user = userSession);
    this.testService.getProducts().subscribe((data: {}) => {
      console.log(data);
    });
    // this.testService.helloWorld().subscribe(
    //       s => console.log("Return:", s),
    //       e => console.log("Error:", e),
    //       () => console.log("Finally")
    //       );
    this.loadMenu();
   }

  private loadMenu() {
    this.resetPage();
    this.menuListPage1 = this.itemService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter);
    this.verifyHasMore(this.size, this.pageNumber);
    this.menuListPage2 = this.itemService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter);
    this.verifyHasMore(this.size, this.pageNumber);
    this.menuListPage3 = this.itemService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter);
    this.verifyHasMore(this.size, this.pageNumber);
  }

  private resetPage(){
    this.pageNumber = 0;
  }

  private verifyHasMore(size, page) {
    let pagesCount = this.itemService.getPagesCount(size);
    this.more = pagesCount > page;
  }

  public onClickMore() {
    let list = this.itemService.getMenu({pageNumber: this.pageNumber++, size: this.size}, this.filter)
    list.forEach(o => this.menuListPageAdd.push(o));
    this.verifyHasMore(this.size, this.pageNumber);
  }

  public onFilter() {
    this.loadMenu();
  }

  public onAddCart(item: ItemMenu) {
    console.log("item", item);
    console.log("modal", this.modal);
    open();
  }

  public openModalAddItem(item) {
    console.log(item);
    this.modalItem = item;
    this.modalReference = this.modalService.open(this.modal, {ariaLabelledBy: 'modal-basic-title'});
  }

  public onAddItem() {
    for(let i=0; i<this.quantidade; i++){
      this.cartService.addItem(this.modalItem);
    }
    console.log(this.cartService.getItemsCart());
    this.closeModal();
  }

  public closeModal() {
    this.quantidade = 1;
    this.modalItem = _.defaults();
    this.modalReference.close();
  }

}

