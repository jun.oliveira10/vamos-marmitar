import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";

import { User } from '../../model/user';

import * as _ from "lodash";
import { LoginService } from '../../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    
    public registerForm: FormGroup;
    submitted = false;

    model: NgbDateStruct;

    public user: User = _.defaults();
    public registerId: string = null;

    constructor(private loginSerivce: LoginService,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute
    ) {
            this.user.email = "jun.oliveira10@gmail.com";
            this.user.password = "1";
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            inputEmail: [null, [Validators.required, Validators.email]],
            inputPassword: [null, [Validators.required]]
        });
        this.registerId = this.route.snapshot.queryParamMap.get("registerId");
        if(this.registerId) {
            this.loginSerivce.validateUser(this.registerId);
        }
    }

    get f() { return this.registerForm.controls; }

    public onClickRegisto() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        this.loginSerivce.login(this.user); 
    }



}

