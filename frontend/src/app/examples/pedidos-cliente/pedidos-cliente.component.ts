import { Component, OnInit } from '@angular/core';

import { Pedido }  from '../../model/pedido';
import { PedidoService }  from '../../service/pedido.service';

@Component({
  selector: 'app-pedidos-cliente',
  templateUrl: './pedidos-cliente.component.html',
  styleUrls: ['./pedidos-cliente.component.scss']
})
export class PedidosClienteComponent implements OnInit {

  public pedidos: Pedido[] = [];

  constructor(private pedidoService: PedidoService) {
    this.pedidos = pedidoService.getPedidos();
   }

  ngOnInit() {
  }

  public onClickPreparar() {
    console.log("onClickPreparar");
  }



}
