import { Component, OnInit } from '@angular/core';

import { Pedido }  from '../../model/pedido';
import { PedidoService }  from '../../service/pedido.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent implements OnInit {

  public pedidos: Pedido[] = [];

  constructor(private pedidoService: PedidoService) {
    this.pedidos = pedidoService.getPedidos();
   }

  ngOnInit() {
  }

  public onClickPreparar() {
    console.log("onClickPreparar");
  }



}
