import { Component, OnInit } from '@angular/core';

import { NgxSpinnerService } from 'ngx-spinner';
import * as _ from 'lodash';

import { EmailService } from '../../service/email.service';
import { EmailAdmin } from '../../model/email-admin';
import { MessageService } from '../../service/message.service';

@Component({
  selector: 'app-nos',
  templateUrl: './nos.component.html',
  styleUrls: ['./nos.component.scss']
})
export class NosComponent implements OnInit {

  public email: EmailAdmin = _.defaults();

  constructor(private emailService: EmailService, 
    private messageService: MessageService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  public enviarEmail() {
    this.spinner.show();
    this.emailService.sendMailToAdmin(this.email)
    .subscribe(ret => {
      this.messageService.addMessageSuccess("Email enviado com sucesso.");
      this.spinner.hide();
    });
  }

}
