import { Component, OnInit } from '@angular/core';

import {NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ItemMenu }  from '../../model/item-menu';
import { ItemService }  from '../../service/item.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  public itemModal: ItemMenu = _.defaults();
  public isNew: boolean = false;
  public itens: ItemMenu[] = [];
  private page: any = { 
                        "pageNumber": 1,
                        "size": 10
                      };

  modalReference: NgbModalRef;

  closeResult: string;

  constructor(private itemService: ItemService,
    private modalService: NgbModal) {
    this.refresh();
   }

  ngOnInit() {
  }

  private refresh() {
    this.itens = this.itemService.getMenu(this.page, null);
  }

   public onClickNovo() {
     this.isNew = true;
     this.itemService.addItem({...this.itemModal});
     this.refresh();
     this.itemModal = undefined;
     this.modalReference.close();
   }

  public onClickDetalhe(item, content) {
    this.isNew = false;
    this.itemModal = item;
    this.open(content);
  }

  public onClickNew(content) {
    this.isNew = true;
    this.itemModal = _.defaults();
    this.open(content);
  }

  public onClickEcluir(item) {
    this.itemService.removeItem(item);
    this.refresh();
  }

  private open(content) {
    this.modalReference = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

}
