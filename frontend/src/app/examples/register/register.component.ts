import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { User } from '../../model/user';

import * as _ from "lodash";
import { LoginService } from '../../service/login.service';
import { match } from 'minimatch';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public registerForm: FormGroup;
    submitted = false;

    model: NgbDateStruct;

    public focus:any;
    public user: User = _.defaults();

    constructor(private loginSerivce: LoginService,
                private formBuilder: FormBuilder
        ) {
            this.user.email = "jun.oliveira10@gmail.com";
            this.user.password = "1";
        }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            inputNome: [null, [Validators.required]],
            inputEmail: [null, [Validators.required, Validators.email]],
            inputSenha: ['', [Validators.required]],
            inputConfirmaSenha: ['', [Validators.required]]
        }, {validator: this.checkPasswords});
    }

    get f() { return this.registerForm.controls; }

    public onClickRegisto() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        this.loginSerivce.newUser(this.user); 
    }

    checkPasswords(group: FormGroup) {
        let objPass = group.get('inputSenha');
        let objConfirmPass = group.get('inputConfirmaSenha');

        let pass = objPass.value;
        let confirmPass = objConfirmPass.value;

        const regexPass = new RegExp("(?=.{6,})");

        if(!regexPass.test(pass)) {
            objPass.setErrors({ notSecure: true });
        }

        if(pass !== confirmPass) {
            objConfirmPass.setErrors({ notSame: true });
        }

    }

}
