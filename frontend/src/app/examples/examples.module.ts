import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { CardapioComponent } from './cardapio/cardapio.component';
import { DiasComponent } from './dias/dias.component';
import { NosComponent } from './nos/nos.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { PedidosClienteComponent } from './pedidos-cliente/pedidos-cliente.component';
import { ItemComponent } from './item/item.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        TranslateModule
    ],
    declarations: [
        LandingComponent,
        ProfileComponent,
        CardapioComponent,
        DiasComponent,
        NosComponent,
        LoginComponent,
        RegisterComponent,
        PedidosComponent,
        PedidosClienteComponent,
        ItemComponent
    ],
    entryComponents: [
    ]
})
export class ExamplesModule { }
