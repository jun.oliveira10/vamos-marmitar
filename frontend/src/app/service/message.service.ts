import { Injectable } from '@angular/core';

import { Alert } from '../model/alert';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

   private messages: Alert[] = [];

   private loaderSubject = new Subject<Alert[]>();
   loaderMessage = this.loaderSubject.asObservable();

   constructor() {

   }

   public getMessages(): Alert[] {
       return this.messages;
   }

   private addMessage(type: string, message: string, cssClass: string) {
       let messageObj = this.messages.push({
        type: type,
        message: message,
        cssClass: cssClass
    });
    this.loaderSubject.next(<Alert[]> this.messages);
    setTimeout(() => this.clearMessages(), 10000);
    return messageObj;
    }

    public addMessageError(exception) {
        console.log(exception);
        let message = exception.error.message;
        console.log(message);
        return this.addMessage("danger", message, "alertMessage-error");
    }

    public addMessageSuccess(message: string) {
        return this.addMessage("success", message, "alertMessage-success");
    }

    public addMessageInfo(message: string) {
        return this.addMessage("info", message, "alertMessage-info");
    }

    public addMessageWarning(message: string) {
        return this.addMessage("warning", message, "alertMessage-warning");
    }

    public clearMessages() {
        this.messages = [];
        this.loaderSubject.next(<Alert[]> this.messages);
        return this.messages;
    }

}
