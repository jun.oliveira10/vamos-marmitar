import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  public api;
  public httpOptions;

  constructor() {
    let isDevEnvironment: boolean = true;
    let host;
    if(isDevEnvironment) {
      host = "http://localhost:4200/api/";
    } else {
      host = "https://vamos-marmitar-backend.herokuapp.com/";
    }
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': host
      })
    };
    this.api = host;
  }

}
