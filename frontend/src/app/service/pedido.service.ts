import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Pedido } from '../model/pedido';
import { BaseService } from './base.service';


@Injectable({
  providedIn: 'root'
})
export class PedidoService extends BaseService {

  private pedidos: Pedido[] = [];
  private path = "Order";

  constructor(private http: HttpClient) {
    super();
   }

   public getPedidos(): Pedido[] {
    this.preenchePedidos();
    return this.pedidos;
  }

  public novoPedido(pedido: Pedido) {
    return this.http.post<Pedido>(this.api + this.path + '/new', JSON.stringify(pedido), this.httpOptions);
  }

  public prepararPedido(pedido: Pedido) {
    return this.http.post<Pedido>(this.api + this.path + '/prepare', JSON.stringify(pedido), this.httpOptions);
  }

  public finalizarPedido(pedido: Pedido) {
    return this.http.post<Pedido>(this.api + this.path + '/finish', JSON.stringify(pedido), this.httpOptions);
  }

   private getPedidosService() {
    return this.http.get<Pedido[]>(this.api + this.path + '/getAll');
  }

  private preenchePedidos() {
    this.getPedidosService().subscribe(obj => this.pedidos = obj);
  }

}
