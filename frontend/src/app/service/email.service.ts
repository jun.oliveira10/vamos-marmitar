import { Injectable } from '@angular/core';

import { EmailAdmin } from '../model/email-admin';
import { BaseService } from './base.service';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmailService extends BaseService{

  private path = "Message";

  constructor(private http: HttpClient) {
    super();
   }

  public sendBackupMail() {
    return this.http.get<boolean>(this.api + this.path + '/mail-backup');
  }

  public sendMailToAdmin(email: EmailAdmin) {
    return this.http.post<EmailAdmin>(this.api + this.path + '/mail-admin', JSON.stringify(email), this.httpOptions);
  }

}
