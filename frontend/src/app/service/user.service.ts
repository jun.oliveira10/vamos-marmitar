import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../model/user';
import { BaseService } from './base.service';


@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

  private path = "User";

  constructor(private http: HttpClient) {
    super();
   }

  public getUsers() {
    return this.http.get<User[]>(this.api + this.path + '/getAll');
  }

  public addUser(user: User) {
    return this.http.post<User>(this.api + this.path + '/new', JSON.stringify(user), this.httpOptions);
  }

  public login(user: User) {
    return this.http.post<User>(this.api + this.path + '/login', JSON.stringify(user), this.httpOptions);
  }

  public newUser(user: User) {
    return this.http.post<User>(this.api + this.path + '/new', JSON.stringify(user), this.httpOptions);
  }

  public validateUser(registerId: String) {
    return this.http.post<User>(this.api + this.path + '/validate?registerId=' + registerId, "", this.httpOptions);
  }

}
