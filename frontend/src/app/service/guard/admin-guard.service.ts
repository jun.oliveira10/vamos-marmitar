import { Injectable, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { LoginService } from '../login.service';
import { User } from '../../model/user';

import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate, OnInit {

    private userSession: User = _.defaults();

    constructor(private authService: LoginService, private router: Router){}

    ngOnInit() {
    }

    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
        this.userSession = this.authService.getUserLogin();
        console.log("Can activate", this.userSession);
        if(this.userSession.type === "ADMIN") {
          return true;
        } else {
          return this.router.parseUrl("/login");
        }
        
    }
    
  }