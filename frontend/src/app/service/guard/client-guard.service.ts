import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { LoginService } from '../login.service';
import { User } from '../../model/user';

@Injectable({
  providedIn: 'root'
})
export class ClientGuard implements CanActivate {

    private userSession: User;

    constructor(private authService: LoginService, private router: Router){}
  
    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
        this.userSession = this.authService.getUserLogin();
        if(this.userSession.type === "CLIENT") {
          return true;
        } else {
          return this.router.parseUrl("/login");
        }
        
    }
    
  }