import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { ItemMenu } from '../model/item-menu';


@Injectable({
  providedIn: 'root'
})
export class ItemService extends BaseService{

  private path = "Item";

  private itens: ItemMenu[] = [];

  constructor(private http: HttpClient) {
      super();
      this.preencheListaItens();
   }

   private getItens() {
    return this.http.get<ItemMenu[]>(this.api + this.path + '/getAll');
  }

  private preencheListaItens() {
    this.getItens().subscribe(obj => this.itens = obj);
  }

  public addItem(item: ItemMenu) {
    this.itens.push(item);
  }

  public removeItem(item: ItemMenu) {
    this.itens = this.itens.filter(i => i.id !== item.id);
  }

  public getMenu(page, filter) {
    let initial = page.pageNumber * page.size;
    let limit = initial + page.size;

    let listFiltered = !!filter 
    ? this.itens.filter(o => {
        console.log("title", o.title);
        console.log("filter", filter);
        console.log("condition", o.title.indexOf(filter) !== -1);
        return  o.title.indexOf(filter) !== -1
    }) 
    : this.itens;
    let pagedList = listFiltered.slice(initial, limit);
    return pagedList;
}

  public getPagesCount(size) {
    return this.itens.length / size;
  }

}
