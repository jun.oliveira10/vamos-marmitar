import { Injectable } from '@angular/core';
import {PlatformLocation } from '@angular/common';

import { ItemMenu } from '../model/item-menu';



@Injectable({
  providedIn: 'root'
})
export class CartService {

    public itemsCart: ItemMenu[] = [];

    public getItemsCart() {
        return this.itemsCart;
    }

    public addItem(item: ItemMenu) {
        return this.itemsCart.push(item);
    }

    public removeItem(item: ItemMenu) {
        return this.itemsCart = this.itemsCart.filter(itemFun => itemFun.id !== item.id);
    }

}
