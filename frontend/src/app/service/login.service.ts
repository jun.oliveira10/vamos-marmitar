import { Injectable } from '@angular/core';

import { User } from '../model/user';
import { UserService } from './user.service';
import { Subject, Observable } from 'rxjs';
import { MessageService } from './message.service';
import { Router } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private userSession: any;

  private loaderSubject = new Subject<User>();
  loaderUser = this.loaderSubject.asObservable();

  constructor(private userService: UserService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private router: Router
    ) {
    this.userSession = this.getUserLogoff();
   }

   public getUserSession(): Observable<User> {
    return this.loaderUser;
  }

  public getUserLogin(): User {
    return this.userSession;
  }

   public login(user: User) {
    this.spinner.show();
    this.userService.login(user).subscribe(
      obj => {
        this.userSession = obj;
        this.loaderSubject.next(<User> this.userSession);
        this.spinner.hide();
        if(this.userSession.type === "ADMIN") {
            this.router.navigateByUrl("/pedidos");
        } else if(this.userSession.type === "CLIENT") {
            this.router.navigateByUrl("/pedidos-cliente");
        }
    }, error => {
      this.spinner.hide();
      this.messageService.addMessageError(error);
    })
   }

   public logoff() {
    this.spinner.show();
    this.userSession = this.getUserLogoff();
    this.loaderSubject.next(<User> this.userSession);
    this.router.navigateByUrl("/home");
    this.spinner.hide();
   }

   public newUser(user: User) {
    this.spinner.show();
    this.userService.newUser(user).subscribe(
      obj => {
        this.messageService.addMessageSuccess("Enviamos um email de confirmação, utilize o link para validar o acesso.");
        this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.messageService.addMessageError(error);      
    })
   }

   public validateUser(registerId: string) {
    this.spinner.show();
    this.userService.validateUser(registerId).subscribe(
      obj => {
        this.userSession = obj;
        this.loaderSubject.next(<User> this.userSession);
        this.router.navigateByUrl("/pedidos-cliente");
        this.messageService.addMessageSuccess("Usuário validado com sucesso");
        this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.messageService.addMessageError(error);      
    })
   }

  public getUserLogoff(): User{
    return {
      "id": "",
      "name": "",
      "type": "LOGOFF",
      "email": "vamos-marmitar@gmail.com",
      "password": ""
    };;
  }

}
