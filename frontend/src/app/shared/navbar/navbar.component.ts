import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from '../../service/login.service';
import { User } from '../../model/user';
import { ItemMenu } from '../../model/item-menu';
import { CartService } from '../../service/cart.service';
import { Alert } from '../../model/alert';
import { MessageService } from '../../service/message.service';

import * as _ from "lodash";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    
    @ViewChild("cartModal") modal: ElementRef;
    private isCartFilled: boolean = false;

    public user: User = _.defaults();
    public itemsCart: ItemMenu[] = [];
    public alerts: Alert[] = [];
    private toggleButton: any;
    private sidebarVisible: boolean;

    modalReference: NgbModalRef;

    constructor(public location: Location, 
        private element : ElementRef,
        private loginService : LoginService,
        private cartService: CartService,
        private modalService: NgbModal,
        private messageService: MessageService,
        ) {
            this.user = this.loginService.getUserLogoff();
         }


    ngOnInit() {
        this.messageService.loaderMessage.subscribe(messages => this.alerts = messages);
        this.loginService.getUserSession().subscribe(user => this.user = user);
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    }

    onLogoff() {
        this.loginService.logoff();
    }
    
    close(message: Alert) {
        this.alerts.splice(this.alerts.indexOf(message), 1);
    }

    public onClickCart() {
        console.log("Clicked");
        this.itemsCart = this.cartService.getItemsCart();
        this.cartFilled();
        console.log("itens cart", this.cartFilled());
        this.openCartModal();
    }

    public cartFilled() {
        this.isCartFilled = this.itemsCart.length > 0;
    }

    private openCartModal() {
        this.modalReference = this.modalService.open(this.modal, {ariaLabelledBy: 'modal-basic-title'});
      }

    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        // console.log(toggleButton, 'toggle');

        setTimeout(function(){
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        // this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };
    isHome() {
      var titlee = this.location.prepareExternalUrl(this.location.path());
      if(titlee.charAt(0) === '#'){
          titlee = titlee.slice( 1 );
      }
        if( titlee === '/home' ) {
            return true;
        }
        else {
            return false;
        }
    }
    isDocumentation() {
      var titlee = this.location.prepareExternalUrl(this.location.path());
      if(titlee.charAt(0) === '#'){
          titlee = titlee.slice( 1 );
      }
        if( titlee === '/documentation' ) {
            return true;
        }
        else {
            return false;
        }
    }
}
