export interface Alert {
    type: string;
    message: string;
    cssClass: string;
  }