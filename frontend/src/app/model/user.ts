export interface User {

    id: string;
    password: string;
    email: string;
    type: string;
    name: string;

}