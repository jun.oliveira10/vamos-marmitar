export interface EmailAdmin {

    id: string;
    password: string;
    email: string;
    type: string;
    name: string;

}