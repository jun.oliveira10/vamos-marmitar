import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components/components.component';
import { ProfileComponent } from './examples/profile/profile.component';
import { LandingComponent } from './examples/landing/landing.component';

import { CardapioComponent } from './examples/cardapio/cardapio.component';
import { DiasComponent } from './examples/dias/dias.component';
import { NosComponent } from './examples/nos/nos.component';
import { LoginComponent } from './examples/login/login.component';
import { RegisterComponent } from './examples/register/register.component';
import { PedidosComponent } from './examples/pedidos/pedidos.component';
import { PedidosClienteComponent } from './examples/pedidos-cliente/pedidos-cliente.component';
import { ItemComponent } from './examples/item/item.component';

import { AdminGuard } from './service/guard/admin-guard.service';
import { ClientGuard } from './service/guard/client-guard.service';

import {TranslateModule} from '@ngx-translate/core';

const routes: Routes =[
    { 
      path: '', 
      redirectTo: 'home', 
      pathMatch: 'full' 
    },
    { 
      path: 'home',
      component: ComponentsComponent 
    },
    {
       path: 'user-profile',
       component: ProfileComponent 
    },
    { 
      path: 'landing',
      component: LandingComponent 
    },
    { 
      path: 'cardapio',
      component: CardapioComponent 
    },
    { 
      path: 'nos',
      component: NosComponent 
    },
    { 
      path: 'dias',
      component: DiasComponent 
    },
    { 
      path: 'login',
      component: LoginComponent 
    },
    { 
      path: 'register',
      component: RegisterComponent 
    },
    { 
      path: 'pedidos-cliente',
      component: PedidosClienteComponent,
      canActivate: [ClientGuard]
    },
    { 
      path: 'pedidos',
      component: PedidosComponent,
      canActivate: [AdminGuard]
    },
    { 
      path: 'item',
      component: ItemComponent,
      canActivate: [AdminGuard]
    }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    TranslateModule,
    RouterModule.forRoot(routes,{
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
